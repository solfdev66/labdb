/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sittpol.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class UpdateDatabase {
    public static void main(String[] args) {
        
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee1.db";
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        String sql = "UPDATE category SET category_name=? WHERE category_id=?";
        try {
           PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, "MyCoffee");
           stmt.setInt(2, 1);
           int status = stmt.executeUpdate();
            
           
        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}


