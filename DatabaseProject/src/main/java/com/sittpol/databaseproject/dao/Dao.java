/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sittpol.databaseproject.dao;

import com.sittpol.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll(String user_name_like_u_, String user_name_asc_user_gender_desc);
    T save(T obj);
    T update(T obj);
    int delete(T obj);
    List<T> getAll(String where, String order);
} 
